
# This repository is moved to GitHub and it is open sources now
Link to [source code in GitHub](https://github.com/dostonhamrakulov/Website-for-University-courses)

# Doston Hamrakulov

## Master's Degree in Web Engineering at Chemnitz University of Technology

## Academic year 2018/19 - course materials.

## Author
**Doston Hamrakulov**

>*Computer Systems, Riga Technical University, Riga, Latvia*
>*Web Engineering, Chemnitz University of Technology, Germany*



[dostonhamrakulov.github.io/Website-for-University-courses/](https://dostonhamrakulov.github.io/Website-for-University-courses/)
