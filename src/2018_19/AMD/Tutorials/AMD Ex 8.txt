DROP TRIGGER IF EXISTS numbers_alter ON numbers;

CREATE TRIGGER numbers_alter AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON numbers
  FOR EACH STATEMENT
    WHEN (pg_trigger_depth() = 0)
      EXECUTE PROCEDURE numbers_query();


CREATE OR REPLACE FUNCTION numbers_query() RETURNS TRIGGER AS $$
  BEGIN
    INSERT INTO numbers_log_query VALUES (current_query());
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER numbers_alter AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON numbers
  FOR EACH STATEMENT -- could be omitted, as this is the default
    EXECUTE PROCEDURE numbers_query();


CREATE TABLE IF NOT EXISTS numbers_log_query (query TEXT NOT NULL);


CREATE OR REPLACE FUNCTION numbers_more() RETURNS TRIGGER AS $$
  BEGIN
    INSERT INTO numbers VALUES (NEW.number + 1);
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER numbers_insert_after AFTER INSERT ON numbers
  FOR EACH ROW
    WHEN (pg_trigger_depth() = 0)
      EXECUTE PROCEDURE numbers_more();
     
     
DROP TRIGGER IF EXISTS numbers_insert_after ON numbers;

CREATE OR REPLACE FUNCTION numbers_double() RETURNS TRIGGER AS $$
  BEGIN
    NEW.number = NEW.number * 2; -- manipulate value
    RETURN NEW;                  -- return new value
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER numbers_insert_before_double BEFORE INSERT ON numbers
  FOR EACH ROW
    EXECUTE PROCEDURE numbers_double();


CREATE OR REPLACE FUNCTION numbers_more() RETURNS TRIGGER AS $$
  BEGIN
    INSERT INTO numbers VALUES (NEW.number + 1);
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER numbers_insert_after AFTER INSERT ON numbers
  FOR EACH ROW
    EXECUTE PROCEDURE numbers_more();
    
TRUNCATE TABLE numbers;

INSERT INTO numbers VALUES (1), (100);


CREATE OR REPLACE FUNCTION numbers_stop() RETURNS TRIGGER AS $$
  BEGIN
    IF (SELECT count(*) FROM numbers) >= 50 THEN
      RETURN NULL; -- do nothing and stop all further trigger from firing
    END IF;
    RETURN NEW;    -- return it, as this is what is inserted
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER numbers_insert_before BEFORE INSERT ON numbers
  FOR EACH ROW
    EXECUTE PROCEDURE numbers_stop();
    
CREATE TRIGGER numbers_insert AFTER INSERT ON numbers
  FOR EACH ROW
    EXECUTE PROCEDURE numbers_copy();

INSERT INTO numbers VALUES (1);

DELETE FROM numbers WHERE number < 5;

INSERT INTO numbers VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9);

SELECT * FROM numbers;
SELECT * FROM numbers_log;

CREATE OR REPLACE FUNCTION numbers_copy() RETURNS TRIGGER AS $$
  BEGIN
    INSERT INTO numbers_log VALUES (NEW.number);
    RETURN NULL; -- result is ignored since this will be an AFTER trigger
  END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS numbers (number INTEGER NOT NULL);
CREATE TABLE IF NOT EXISTS numbers_log (number INTEGER NOT NULL);